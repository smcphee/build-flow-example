---
#
stages:
  - build
  - package
  - prepare
  - release
  - deploy

variables:
  MAVEN_OPTS: -Dmaven.repo.local=.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Djava.awt.headless=true
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"
  MAVEN_CLI_OPTS_NO_TESTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -Dsonar.skip=true -DdeployAtEnd=true -DskipTests -Darguments=-DskipTests -DskipITs -Darguments=-DskipITs -Darguments=-Dsonar.skip=true"
  PBRANCH: "develop"

default:
  cache:
    key: "$CI_COMMIT_REF_SLUG"
    paths:
      - .m2/repository

  before_script:
    - export VERSION=$(cat .bumpversion.cfg | grep current_version | awk '{ print $3 }')
    - echo Docker artifact is $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA with VERSION=$VERSION

# container build template
.builder: &builder
  image: registry.gitlab.com/smcphee/dockerfiles/corretto11:stable
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
    paths:
      - target/*.jar

# clean compile test
compile-test-verify:
  <<: *builder
  stage: build
  script:
    - mvn ${MAVEN_CLI_OPTS} clean compile test verify
  except:
    # don't release latest when branch was patched or version tags
    variables:
      - $CI_COMMIT_TITLE =~ /^\[patch\].*/
    refs:
      - /^v[\d]+\.[\d]+(\.[\d]+)?/

# package the jar file for release step
package:
  <<: *builder
  stage: package
  script:
    - mvn clean install -update-snapshots ${MAVEN_CLI_OPTS_NO_TESTS}
  except:
    # don't release latest when branch was patched or version tags
    variables:
      - $CI_COMMIT_TITLE =~ /^\[patch\].*/
    refs:
      - master
      - /^v[\d]+\.[\d]+(\.[\d]+)?/

docker-develop:
  stage: release
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME-latest --build-arg JAR_FILE=target/$CI_PROJECT_NAME.jar
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:latest --build-arg JAR_FILE=target/$CI_PROJECT_NAME.jar
  only:
    refs:
      - develop
  except:
    # don't release latest when branch was patched
    variables:
      - $CI_COMMIT_TITLE =~ /^\[patch\].*/

docker-feature:
  stage: release
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA --build-arg JAR_FILE=target/$CI_PROJECT_NAME.jar
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME-latest --build-arg JAR_FILE=target/$CI_PROJECT_NAME.jar
  except:
    # don't release latest when branch was patched or 1version tags
    variables:
      - $CI_COMMIT_TITLE =~ /^\[patch\].*/
    refs:
      - develop
      - master
      - /^v[\d]+\.[\d]+(\.[\d]+)?/

tag-release:
  <<: *builder
  stage: prepare
  script:
    - echo "will now create version v$VERSION in a tag"
    - "curl -v --request POST --header \"PRIVATE-TOKEN:$API_ACCESS_TOKEN\" \"$CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/tags?tag_name=v$VERSION&ref=master\""
  only:
    refs:
      - master
  except:
    # don't release latest when branch was patched or version tags
    variables:
      - $CI_COMMIT_TITLE =~ /^\[patch\].*/

# clean compile test on a production tag
production-test:
  <<: *builder
  stage: build
  script:
    - mvn ${MAVEN_CLI_OPTS} clean compile test
  only:
    refs:
      - /^v[\d]+\.[\d]+(\.[\d]+)?/
  except:
    variables:
      - $CI_COMMIT_TITLE =~ /^\[patch\].*/

# package the jar file for release step
production-package:
  <<: *builder
  stage: package
  script:
    - mvn clean install ${MAVEN_CLI_OPTS_NO_TESTS}
  only:
    refs:
      # only release from protected version tags
      - /^v[\d]+\.[\d]+(\.[\d]+)?/

production-docker-release:
  stage: release
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:v$VERSION --build-arg JAR_FILE=target/$CI_PROJECT_NAME.jar
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:stable --build-arg JAR_FILE=target/$CI_PROJECT_NAME.jar
  only:
    refs:
      # only release from protected version tags
      - /^v[\d]+\.[\d]+(\.[\d]+)?/

production-increment-patch:
  <<: *builder
  stage: release
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
    paths:
      - payload.log
  script:
    - echo "will now bumpversion from $VERSION"
    - bumpversion --no-commit patch
    - export NEWVERSION=$(cat .bumpversion.cfg | grep current_version | awk '{ print $3 }')
    - export COMMITMESSAGE="[patch] next version v$NEWVERSION from $VERSION onto $PBRANCH from $CI_BUILD_REF_NAME"
    - echo "$COMMITMESSAGE"
    - export CONTENT_BUMPVER=$(cat .bumpversion.cfg | base64 -w 0)
    - export CONTENT_MVNCFG=$(cat .mvn/maven.config | base64 -w 0)
    - echo "{ \"branch\":\"$PBRANCH\", \"commit_message\":\"$COMMITMESSAGE\", \"actions\":[  { \"action\":\"update\", \"file_path\":\".bumpversion.cfg\", \"encoding\":\"base64\", \"content\":\"$CONTENT_BUMPVER\" },  { \"action\":\"update\", \"file_path\":\".mvn/maven.config\", \"encoding\":\"base64\", \"content\":\"$CONTENT_MVNCFG\" } ] }" > payload.log
    - export PAYLOAD=$(cat payload.log)
    - "curl -v --request POST --header \"PRIVATE-TOKEN:$API_ACCESS_TOKEN\" --header \"Content-Type:application/json\" --data \"$PAYLOAD\" \"$CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/commits\""
  only:
    refs:
      # only release from protected version tags
      - /^v[\d]+\.[\d]+(\.[\d]+)?/
